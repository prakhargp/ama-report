# Git command to change previous commit

> git commit --amend

This command can be used to modify the most recent commit.
It lets you combine staged changes with the previous commit instead of creating an entirely new commit.
It can also be used to simply edit the previous commit message without changing its snapshot.
But, amending does not just alter the most recent commit, it replaces it entirely.
To Git, it will look like a brand new commit.

&nbsp;

# Yield keyword in ruby

Ruby has a concept of Block.

- A block consists of chunks of code.

- You assign a name to a block.

- The code in the block is always enclosed within braces ({}).

- A block is always invoked from a function with the same name as that of the block. This means that if you have a block with the name test, then you use the function test to invoke this block.

- You invoke a block by using the yield statement.

_Example_ :

        def test
        puts "You are in the method"
        yield
        puts "You are again back to the method"
        yield
        end
        test {puts "You are in the block"}

_Output_ :

        You are in the method
        You are in the block
        You are again back to the method
        You are in the block

&nbsp;
We can also pass parameters with the yield statement.

_Example_ :

        def test
        yield 5
        puts "You are in the method test"
        yield 100
        end
        test {|i| puts "You are in the block #{i}"}

_Output_ :

        You are in the block 5
        You are in the method test
        You are in the block 100

&nbsp;

# Proc in Ruby

Ruby introduces procs so that we are able to pass blocks around.
Proc objects are blocks of code that have been bound to a set of local variables.
Once bound, the code may be called in different contexts and still access those variables.

        factor = Proc.new {|n| print n*2}

or

        factor = proc {|n| print n*2}

We precede the argument with & so that ruby knows that this is a proc and not a variable.

        def my_each(&block )
            self.length.times do |i|
            # and now we can call our new Proc like normal
            block.call( self[i] )
            end
        end
        [1,2,3].my_each { |i| puts i*2 }

&converts the block into a proc so we treat the block as a proc inside our method.
We no longer use yield since it is possible to pass more than one proc to a method, we have to say which one we are calling.
There are different ways of calling procs in our methods. Using call, () or using [].

&nbsp;

# Order of SQL query Execution

1. **_FROM and JOINs_**

- The FROM clause, and subsequent JOINs are first executed to determine the total working set of data that is being queried. This includes subqueries in this clause, and can cause temporary tables to be created under the hood containing all the columns and rows of the tables being joined.

2. **_WHERE_**

- Once we have the total working set of data, the first-pass WHERE constraints are applied to the individual rows, and rows that do not satisfy the constraint are discarded. Each of the constraints can only access columns directly from the tables requested in the FROM clause. Aliases in the SELECT part of the query are not accessible in most databases since they may include expressions dependent on parts of the query that have not yet executed.

3. **_GROUP BY_**

- The remaining rows after the WHERE constraints are applied are then grouped based on common values in the column specified in the GROUP BY clause. As a result of the grouping, there will only be as many rows as there are unique values in that column. Implicitly, this means that you should only need to use this when you have aggregate functions in your query.

4. **_HAVING_**

- If the query has a GROUP BY clause, then the constraints in the HAVING clause are then applied to the grouped rows, discard the grouped rows that don't satisfy the constraint. Like the WHERE clause, aliases are also not accessible from this step in most databases.

5. **_SELECT_**

- Any expressions in the SELECT part of the query are finally computed.

6. **_DISTINCT_**

- Of the remaining rows, rows with duplicate values in the column marked as DISTINCT will be discarded.

7. **_ORDER BY_**

- If an order is specified by the ORDER BY clause, the rows are then sorted by the specified data in either ascending or descending order. Since all the expressions in the SELECT part of the query have been computed, you can reference aliases in this clause.

8. **_LIMIT / OFFSET_**

- Finally, the rows that fall outside the range specified by the LIMIT and OFFSET are discarded, leaving the final set of rows to be returned from the query.

&nbsp;

# How many instances of class in ruby ?

One way to do is to keep track of it as and when you create new instances.

        class Project

            @@count = 0
            @@instances = []

            def initialize(options)
                @@count += 1
                @@instances << self
            end

            def self.all
                @@instances.inspect
            end

            def self.count
                @@count
            end

        end

If you want to use ObjectSpace, then its

        def self.count
            ObjectSpace.each_object(self).count
        end

        def self.all
            ObjectSpace.each_object(self).to_a
        end

&nbsp;

# HTML vs HTML5:

<img src="a.png"
     alt="Markdown Monster icon"
     style="width: 60%" />

&nbsp;     

# IS keyword in SQL

In databases, NULL is unknown, not applicable or missing information, therefore, you cannot use the comparison operators (=, >,<, etc.,) to check whether a value is NULL or not.

Fortunately, SQL provides the  IS operator to check whether a value is NULL.

    SELECT companyName, fax
    FROM suppliers
    WHERE fax IS NULL;

    SELECT companyName, fax
    FROM suppliers
    WHERE fax IS NOT NULL;

&nbsp;

# Types of iterators in ruby

1. ### **Ruby Each Iterator**
- The Ruby each iterator returns all the elements from a hash or array.

**Syntax :**

        (collection).each do |variable|  
        code...  
        end

**Example :**

        (1...5).each do |i|   
        puts i   
        end           

**Output :**

        1
        2
        3
        4
        
2. ### **Ruby Times Iterator**
- A loop is executed specified number of times by the times iterator. Loop will start from zero till one less than specified number.

**Syntax :**

        x.times do |variable|  
        code...  
        end          

**Example :**

        5.times do |n|   
        puts n   
        end   

**Output :**

        1
        2
        3
        4
                
3. ### **Ruby Upto Iterator**
- An upto iterator iterates from number x to number y.

**Syntax :**

        x.upto(y) do |variable|  
        code  
        end      

**Example :**

        1.upto(5) do |n|   
        puts n   
        end  

**Output :**

        1
        2
        3
        4  
        5              

4. ### **Ruby Step Iterator**
- A step iterator is used to iterate while skipping over a range.

**Syntax :**

        (controller).step(x) do |variable|  
        code  
        end  

**Example :**

        (10..30).step(5) do |n|   
        puts n   
        end  

**Output :**

        10
        15
        20
        25
        30
        

5. ### **Ruby Each_Line Iterator**
- A each_line iterator is used to iterate over a new line in a string.

**Example :**

        "All\nthe\nwords\nare\nprinted\nin\na\nnew\line.".each_line do |line|   
        puts line   
        end  

**Output :**

        All
        the
        words
        are
        printed
        in
        a
        newline.

&nbsp;

# missing_method in ruby

When we send a message to an object, the object executes the first method it finds on its method lookup path with the same name as the message. If it fails to find any such method, it raises a NoMethodError exception - unless you have provided the object with a method called method_missing. 
method_missing is a method in Ruby that intercepts calls to methods that don’t exist. It handles any messages that an object can’t respond to. The method_missing method is passed with the symbol of the non-existent method, an array of the arguments that were passed in the original call and any block passed to the original method.

***Example :***

        class Dummy  
        def method_missing(m, *args, &block)  
        puts "There's no method called #{m} here -- please try again."  
        end  
        end  
        Dummy.new.anything  

***Output :***

        There's no method called anything here -- please try again. 

&nbsp;

# Send method in ruby

send( ) is an instance method of the Object class. The send method invokes the method identified by symbol, passing it any arguments specified.When the method is identified by a string, the string is converted to a symbol. The first argument to send( ) is the message that you're sending to the object - that is, the name of a method. You can use a string or a symbol, but symbols are preferred. Any remaining arguments are simply passed on to the method.

        class Rubyist
        def welcome(*args)
        "Welcome " + args.join(' ')
        end
        end
        obj = Rubyist.new
        puts(obj.send(:welcome, "famous", "Rubyists"))   # => Welcome famous Rubyists
